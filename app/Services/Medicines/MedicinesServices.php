<?php
namespace Memed\Services\Medicines;

use Memed\Models\Fabricante;
use Memed\Models\Medicamento;
use Memed\Models\TiposMedicamento;
use Memed\Models\TiposTargeta;
use Memed\Models\Valor;
use Memed\Traits\CustomOutMedicines;

class MedicinesServices
{
	use CustomOutMedicines;
	private $medicamento;
	protected $fabricante;
	protected $tiposMedicamento;
	protected $tiposTargeta;
	protected $valor;

	public function __construct(
		Medicamento $medicamento,
		Fabricante $fabricante,
		TiposMedicamento $tiposMedicamento,
		TiposTargeta $tiposTargeta,
		Valor $valor
	)
	{
		$this->medicamento = $medicamento;
		$this->fabricante = $fabricante;
		$this->tiposMedicamento = $tiposMedicamento;
		$this->tiposTargeta = $tiposTargeta;
		$this->valor = $valor;

	}
	public function store($request)
	{
		\DB::beginTransaction();

		try {

			$fabricante = $request->input('manufacturer_id');
			$tipo_medicamento = $request->input('type_id');

			if(!(int)$fabricante) {

				$fabricante = $this->fabricante->create([
					'titulo' => $request->input('manufacturer_id'),
					'fabricante_slug' => $request->input('manufacturer_id')
				])->id;
			}
			if(!(int)$tipo_medicamento) {

				$tipo_medicamento = $this->tiposMedicamento->create([
					'titulo' => $request->input('medical_type_title'),
					'forma_fisica' => $request->input('type_id'),
				])->id;
			}

			$valor = $this->valor->create([
				'preco' => $request->input('price'),
				'preco_maximo' => $request->input('maximum_price'),
				'preco_minimo' => $request->input('minimum_price'),
			]);

			$medicamento = $this->medicamento->create([
				'titulo' => $request->input('title'),
				'descricao' => $request->input('note'),
				'subtitulo' => $request->input('subtitle'),
				'receituario' => 'personal',
				'status' => 'Ativo'
			]);

			$medicamento->pivot_medicamentos()->create([
				'id_medicamento' => $medicamento->id,
				'id_tipo_medicamento' => $tipo_medicamento,
				'id_tipo_targeta' => 2,
				'id_valor' => $valor->id,
				'id_fabricante' => $fabricante,
			]);

			\DB::commit();
			return response()->json(['success' => 'Registro criado com sucesso!']);

		}catch (\Exception $e){
			\DB::rollback();
			return response()->json(['error' => $e->getMessage()]);
		}
	}
	public function update($request)
	{
		\DB::beginTransaction();

		try {

			$medicine = $this->medicamento->where('id',$request->id)
				->where('receituario','personal')
				->first();

			if ($medicine) {

				$fabricante = $request->input('manufacturer_id');
				$tipo_medicamento = $request->input('type_id');

				if (!(int)$fabricante) {

					$fabricante = $this->fabricante->create([
						'titulo' => $request->input('manufacturer_id'),
						'fabricante_slug' => $request->input('manufacturer_id')
					])->id;
				}
				if (!(int)$tipo_medicamento) {

					$tipo_medicamento = $this->tiposMedicamento->create([
						'titulo' => $request->input('medical_type_title'),
						'forma_fisica' => $request->input('type_id'),
					])->id;
				}
				foreach ($medicine->pivot_medicamentos as $pivot_medicamento) {
					$pivot_medicamento->valor->update([
						'preco' => $request->input('price'),
						'preco_maximo' => $request->input('maximum_price'),
						'preco_minimo' => $request->input('minimum_price'),
					]);
			}
				$medicine->update([
					'titulo' => $request->input('title'),
					'descricao' => $request->input('note'),
					'subtitulo' => $request->input('subtitle'),
					'receituario' => 'personal',
					'status' => 'Ativo'
				]);

				$medicine->pivot_medicamentos()->update([
					'id_tipo_medicamento' => $tipo_medicamento,
					'id_tipo_targeta' => 2,
					'id_fabricante' => $fabricante,
				]);

				\DB::commit();
				return response()->json(['success' => 'Registro atualizado com sucesso!']);
			}
		}catch (\Exception $e){
			\DB::rollback();
			return response()->json(['error' => $e->getMessage()]);

		}
	}
	public function delete($request)
	{
		$results = $this->medicamento
			->where('id',$request->medicine_id)
			->where('receituario','personal')->first();

		if ($results) {
			$results->pivot_medicamentos()->delete();
			$results->delete();
			return response()->json(['success' => 'Registro removido com sucesso!']);
		}
		return response()->json(['errors' => ['error' => 'Registro não pode ser removido ou é inexistente!']]);

	}
	public function getAllMedicines($perPage,$search)
	{

		$results = $this->medicamento
			->join('pivot_medicamentos','pivot_medicamentos.id_medicamento','=','medicamentos.id')
			->join('tipos_targetas','tipos_targetas.id','=','pivot_medicamentos.id_tipo_targeta')
			->select(
				'medicamentos.id',
				'titulo',
				'receituario',
				'descricao',
				'principio_ativo',
				'subtitulo',
				'tarja',
				'id_controle_memed'
			)
			->orderByDesc('medicamentos.created_at');

		if ($search) {

			$transforms = $results->where('titulo','like','%'.$search.'%')
				->Orwhere('descricao','like','%'.$search.'%')
				->Orwhere('subtitulo','like','%'.$search.'%');
			return $this->treatmentOutMedicines($transforms->paginate($perPage));
		}
		return $this->treatmentOutMedicines($results->paginate($perPage));

	}
	public function detailsMedicals($id)
	{
		return	$results = $this->medicamento
			->join('pivot_medicamentos','pivot_medicamentos.id_medicamento','=','medicamentos.id')
			->join('tipos_targetas','tipos_targetas.id','=','pivot_medicamentos.id_tipo_targeta')
			->join('valores','valores.id','=','pivot_medicamentos.id_valor')
			->join('tipos_medicamentos','tipos_medicamentos.id','=','pivot_medicamentos.id_tipo_medicamento')
			->join('fabricantes','fabricantes.id','=','pivot_medicamentos.id_fabricante')
			->select(
				'medicamentos.id',
				'tipos_medicamentos.titulo as tipos_titulo',
				'medicamentos.titulo as title',
				'forma_fisica',
				'id_tipo_targeta',
				'id_tipo_medicamento as type_id',
				'tipos_medicamentos.forma_fisica as type_title',
				'id_fabricante as manufacturer_id',
				'fabricantes.titulo as manufacturer_title',
				'receituario',
				'preco as price',
				'preco_maximo as maximum_price',
				'preco_minimo as minimum_price',
				'descricao as note',
				'principio_ativo',
				'subtitulo as subtitle',
				'tarja',
				'id_controle_memed'
			)->where('medicamentos.id',$id)->first();
	}

	public function comboManufacturer($perPage,$search)
	{
		$fabricante = $this->fabricante->select('titulo as display','id as value');

		if ($search) {
			$transforms = $fabricante->where('titulo','like','%'.$search.'%')->paginate($perPage);
			return  $this->treatmentOutCombo($transforms);
		}
		return	 $this->treatmentOutCombo($fabricante->paginate($perPage));
	}
	public function comboTypeMedicines($perPage,$search)
	{
		$tipo_medicamento = $this->tiposMedicamento
			->select('forma_fisica as display','id as value')
			->distinct();

		if ($search) {
			$transforms = $tipo_medicamento->where('forma_fisica','like','%'.$search.'%')->paginate($perPage);
			return	$this->treatmentOutCombo($transforms);
		}
		return	 $this->treatmentOutCombo($tipo_medicamento->paginate($perPage));
	}
}