<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/12/18
 * Time: 18:14
 */

namespace Memed\Services;


use Memed\Models\Fabricante;
use Memed\Models\Medicamento;
use Memed\Models\PivotMedicamento;
use Memed\Models\TiposMedicamento;
use Memed\Models\TiposTargeta;
use Memed\Models\Valor;
use Memed\Util\Util;

class CrudMedicalServices
{

	protected $medicamento;
	protected $pivotMedicamento;
	protected $fabricante;
	protected $tiposMedicamento;
	protected $tiposTargeta;
	protected $valor;
	protected $medicalServices;
	protected $medicalAnvisaServices;

	public function __construct(
		Medicamento $medicamento,
		PivotMedicamento $pivotMedicamento,
		Fabricante $fabricante,
		TiposMedicamento $tiposMedicamento,
		TiposTargeta $tiposTargeta,
		Valor $valor,
		MedicalAnvisaServices $medicalAnvisaServices,
		MedicalMemedServices $medicalServices)
	{
		$this->medicamento = $medicamento;
		$this->pivotMedicamento = $pivotMedicamento;
		$this->fabricante = $fabricante;
		$this->tiposMedicamento = $tiposMedicamento;
		$this->tiposTargeta = $tiposTargeta;
		$this->valor = $valor;
		$this->medicalServices = $medicalServices;
		$this->medicalAnvisaServices = $medicalAnvisaServices;

	}

	public function storeMemed($param)
	{

		try {

			\DB::beginTransaction();

			$data = $this->medicalServices->processCrawlerMemed($param);

			if (isset($data['attributes'])) {

				foreach ($data['attributes'] as $attributes) {

					if (isset($attributes)) {

						$medicamento = $this->medicamento->updateOrCreate([
							'titulo' => Util::converStringUcFirst($attributes['titulo']),
							'controle_especial' => $attributes['controle_especial'],
							'descricao' => Util::converStringUcFirst($attributes['descricao']),
							'status' => $attributes['status'],
							'receituario' => $attributes['receituario'],
							'subtitulo' => Util::converStringUcFirst($attributes['subtitulo']),
							'id_controle_memed' => $attributes['id']
						]);

						$fabricante = $this->fabricante->updateOrCreate([
							'titulo' => Util::converStringUcFirst($attributes['fabricante']),
							'fabricante_slug' => Util::converStringUcFirst($attributes['fabricante_slug'])
						]);
						$tipo_medicamento = $this->tiposMedicamento->updateOrCreate([
							'titulo' => Util::converStringUcFirst($attributes['tipo']),
							'forma_fisica' => Util::converStringUcFirst($attributes['forma_fisica'])
						]);
						$valor = $this->valor->updateOrCreate([
							'preco' => $attributes['preco'],
							'preco_maximo' => $attributes['preco_maximo'],
							'preco_minimo' => $attributes['preco_minimo']
						]);

						$tipo_targeta = $this->tiposTargeta->updateOrCreate([
							'thunbnail' => (isset($attributes['thunbnail']) ? $attributes['thunbnail'] : ''),
							'tarja' => (isset($attributes['tarja']) ? $attributes['tarja'] : '')
						]);

						$this->pivotMedicamento->updateOrCreate([
							'id_medicamento' => $medicamento->id,
							'id_tipo_targeta' => $tipo_targeta->id,
							'id_tipo_medicamento' => $tipo_medicamento->id,
							'id_valor' => $valor->id,
							'id_fabricante' => $fabricante->id
						]);
					}

				}
				\DB::commit();
				return response()->json(['status' => [
					'success' => 'Registros criados com sucesso!',
					'total de registros buscados' => $data['total']]
				]);
			}
			return response()->json(['status' => [
				'error' => 'Erro code status Http => ' . $data['total']]

			]);
		} catch (\Exception $ex){
			\DB::rollback();
			return response()->json(['status' => ['error' => 'Erro interno: ' . $ex->getMessage() . ' (' . $ex->getFile() . '/' . $ex->getLine() . ')']], 500);
		}

	}

	public function storeAnvisa($search)
	{
		try {

			\DB::beginTransaction();

			$results = $this->medicalAnvisaServices->processCrawlerAnvisa($search);
			$count = 0;
			foreach ($results as $attributes) {

				if (isset($attributes['codigoProduto'])) {

					foreach ($attributes['apresentacoes'] as $apresentacoes)
					{

						$medicamento = $this->medicamento->updateOrCreate([
							'titulo' => Util::converStringUcFirst($attributes['nomeComercial']),
							'principio_ativo' =>  Util::converStringUcFirst($attributes['principioAtivo']),
							'subtitulo' => Util::converStringUcFirst($apresentacoes['apresentacao']),
							'descricao' => Util::converStringUcFirst($attributes['principioAtivo'])
								.' / '. Util::converStringUcFirst($apresentacoes['apresentacao']),
							'receituario' => isset($attributes['classesTerapeuticas'][0])?
								Util::converStringUcFirst($attributes['classesTerapeuticas'][0]):null,
						]);

						$fabricante = $this->fabricante->updateOrCreate([
							'titulo' => $this->verifyHasMakers([
								'nacional' => $apresentacoes['fabricantesNacionais'],
								'internacional' => $apresentacoes['fabricantesInternacionais']
							]),
							'fabricante_slug' => $this->verifyHasMakers([
								'nacional' => $apresentacoes['fabricantesNacionais'],
								'internacional' => $apresentacoes['fabricantesInternacionais']
							])
						]);

						$tipo_medicamento = $this->tiposMedicamento->updateOrCreate([
							'titulo' => isset($attributes['classesTerapeuticas'][0])?
								Util::converStringUcFirst($attributes['classesTerapeuticas'][0]):null,
							'forma_fisica' => isset($apresentacoes['formasFarmaceuticas'][0])?
								Util::converStringUcFirst($apresentacoes['formasFarmaceuticas'][0]):null,
						]);

						$tipo_targeta = $this->tiposTargeta->updateOrCreate([
							'thunbnail' => (isset($apresentacoes['thunbnail']) ? $apresentacoes['thunbnail'] : ''),
							'tarja' => (isset($apresentacoes['tarja']) ? $apresentacoes['tarja'] : '')
						]);

						$this->pivotMedicamento->updateOrCreate([
							'id_medicamento' => $medicamento->id,
							'id_tipo_targeta' => $tipo_targeta->id,
							'id_tipo_medicamento' => $tipo_medicamento->id,
							'id_fabricante' => $fabricante->id
						]);
						$count ++;
					}
				}
			}
			\DB::commit();
			return response()->json(['status' => [
				'success' => 'Registros criados com sucesso!',
				'total de registros encontrados na Anvisa ' => $count]
			]);


		}catch (\Exception $ex){
			\DB::rollback();
			return response()->json(['status' => ['error' => 'Erro interno: ' . $ex->getMessage() . ' (' . $ex->getFile() . '/' . $ex->getLine() . ')']], 500);
		}
	}

	public function verifyHasMakers($maker)
	{

		if (isset($maker['nacional'][0]['fabricante'])) {
			return Util::converStringUcFirst($maker['nacional'][0]['fabricante']);
		} elseif (isset($maker['internacional'][0]['fabricante'])) {
			return Util::converStringUcFirst($maker['internacional'][0]['fabricante']);
		} else {
			return 'Sem Fabricantes Capiturados';
		}
	}


}