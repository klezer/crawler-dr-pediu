<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/12/18
 * Time: 16:08
 */

namespace Memed\Services;


use Illuminate\Support\Facades\Artisan;
use Memed\Models\Medicamento;

class MedicamentosServices
{

	protected $medicamento;

	public function __construct( Medicamento $medicamento)
	{
		$this->medicamento = $medicamento;
	}

	public function lists($search = null)
	{
		if ($search) {

			if($this->search($search)->isEmpty()) {

				Artisan::call('command:memed', [
					'char' => $search
				]);

				return $this->search($search);
			}
			return $this->search($search);
		}
	}

	public function search($search)
	{

		$results = $this->medicamento
			->join('pivot_medicamentos','pivot_medicamentos.id_medicamento','=','medicamentos.id')
			->join('tipos_targetas','tipos_targetas.id','=','pivot_medicamentos.id_tipo_targeta')
			->where('titulo','like','%'.$search.'%')
			->select('medicamentos.id','titulo','receituario','descricao','principio_ativo','subtitulo','tarja','medicamentos.id_controle_memed')
			->paginate(6);

		return	$results->map(function ($result){
			return [
				"id" => $result->id,
				"titulo" => $result->titulo,
				"descricao" => $result->descricao,
				"days_validity" => ($this->getVerifyHasValityRecipe($result->receituario))?
					$this->getVerifyHasValityRecipe($result->receituario):30,
				"principio_ativo" => $result->principio_ativo,
				"subtitulo" => $result->subtitulo,
				"tarja" => $result->tarja,
				"id_controle_memed" => $result->id_controle_memed
			];
		});

	}

	public function getVerifyHasValityRecipe($data)
	{
		$path = storage_path() . "/special-medicines/medicals.json";
		$drugs = json_decode(file_get_contents($path), true);

		foreach ($drugs['RECORDS'] as $drug) {

			$pattern = '/\b(\w*'.$drug['title'].'\w*)\b/';
			$subject = $data;
			$results = preg_match($pattern, $subject, $matches);

			if($results) {
				return $drug['days_validity'];
			}
		}
	}

}