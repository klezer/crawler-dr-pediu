<?php


namespace Memed\Services;


use Ixudra\Curl\Facades\Curl;

class MedicalAnvisaServices
{

	const headers = [
		'Accept: application/json, text/plain, */*',
		'Authorization: Guest',
		'Cache-Control: no-cache'
	];
	const url = 'https://consultas.anvisa.gov.br/api/consulta/medicamento/produtos/';

	public function processCrawlerAnvisa($search)
	{
		$data = [];

		$response = Curl::to(self::url)
			->withHeaders(self::headers)
			->withData(array(
				'filter[situacaoRegistro]' => 'V',
				'filter[nomeProduto]' => $search,
				'count' => 100,
				'page' => 1
			))
			->get();

		$results = json_decode($response, true);

		foreach ($results['content'] as $key => $result){
			if(isset($result['ordem'])){
				if(isset($result['processo'])){
					$data[$key] = $this->getInternalMedicalForProcesso($result['processo']['numero']);
				}
			}

		}
		return $data;
	}
	public function getInternalMedicalForProcesso($numero)
	{
		$results = Curl::to(self::url.$numero)
			->withHeaders(self::headers)
			->get();
		return json_decode($results,true);
	}

	public function formatLote($array)
	{
		if(isset($array)){
			return $array;
		}
	}
}