<?php
namespace Memed\Http\Controllers\Medicines;


use Illuminate\Http\Request;
use Memed\Http\Controllers\Controller;
use Memed\Services\Medicines\MedicinesServices;

class MedicinesController extends  Controller
{
	private $medicinesServices;

	public function __construct(MedicinesServices $medicinesServices)
	{
		$this->medicinesServices = $medicinesServices;
	}
	public function store(Request $request)
	{
		return	$this->medicinesServices->store($request);
	}
	public function update(Request $request)
	{
		return $this->medicinesServices->update($request);
	}
	public function getAllMedicines($perPage = 6,$search = null)
	{
		return $this->medicinesServices->getAllMedicines($perPage,$search);
	}
	public function detailsMedicals($id)
	{
		return $this->medicinesServices->detailsMedicals($id);
	}
	public function comboManufacturer($perPage = 6,$search = null)
	{
		return $this->medicinesServices->comboManufacturer($perPage,$search);
	}
	public function comboTypeMedicines($perPage = 6,$search = null)
	{
		return $this->medicinesServices->comboTypeMedicines($perPage,$search);
	}
	public function delete(Request $request)
	{
		return $this->medicinesServices->delete($request);
	}
}