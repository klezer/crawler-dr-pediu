<?php

namespace Memed\Http\Controllers;

use Memed\Services\CrudMedicalServices;
use Memed\Services\MedicamentosServices;

class MemedController extends Controller
{

    protected $crudMedicalServices;
    protected $medicamentosServices;

    public function __construct(CrudMedicalServices $crudMedicalServices,MedicamentosServices $medicamentosServices)
    {
        $this->crudMedicalServices = $crudMedicalServices;
        $this->medicamentosServices = $medicamentosServices;
    }

    public function storeMemed($param)
    {
        return $this->crudMedicalServices->storeMemed($param);
    }
    public function lists( $search = null)
    {
        return $this->medicamentosServices->lists($search);
    }
    public function storeAnvisa($search = null)
    {
    	return $this->crudMedicalServices->storeAnvisa($search);
    }
}
