<?php

namespace Memed\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Concerns\FormatsMessages;

class CreateMedicalsRequest extends FormRequest
{
//	use FormatsMessages;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

	public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|unique:fabricantes',
//            'titulo' => 'required|unique:medicamentos',
        ];
    }
    public function messages()
    {
	    return [
	    	'titulo.unique' => "Este fabricante já se encontra cadastrado!",
	    	'titulo.required' => "O campo fabricante é obrigatório!",
	    ];
    }

}
