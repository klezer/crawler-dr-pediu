<?php


namespace Memed\Traits;


trait CustomOutMedicines
{
	public function treatmentOutMedicines($collection)
	{
		return  ['data' => $collection->map(function ($result) {
			return [
				"id" => $result->id,
				"titulo" => $result->titulo,
				"descricao" => $result->descricao,
				"principio_ativo" => $result->principio_ativo,
				"subtitulo" => $result->subtitulo,
				"tarja" => $result->tarja,
				"receituario" => $result->receituario,
				"id_controle_memed" => $result->id_controle_memed
			];
		}),'total' => $collection->total()];
	}
	public function treatmentOutCombo($collection)
	{
		return  $collection->map(function ($result) {
			return [
				"display" => $result->display,
				"value" => $result->value,
			];
		})->unique('display')->values()->all();
	}
}