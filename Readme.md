## Crawler Dr.Pediu 

Web Service para a capitura dos medicamentos alimentado pelas consultas realizadas nos aplicativos Dr.Pediu (Paciente e Doutor).

## Requerimentos
- PHP 7.2 or higher
- Composer
- MySQL
- Docker Compose

## Authors
- Clezer Ramos

## Contributing
- Clezer Ramos

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
