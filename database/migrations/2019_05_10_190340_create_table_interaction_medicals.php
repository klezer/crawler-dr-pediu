<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInteractionMedicals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interacao_medicamentosas', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('titulo');
	        $table->string('pincipio_ativo_anvisa')->nullable();
	        $table->string('acao')->nullable();
	        $table->text('recomendacao')->nullable();
	        $table->text('efeito_clinico')->nullable();
	        $table->timestamps();
	        $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interacao_medicamentosas');
    }
}
