<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotInteractionMedicals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_interacao_medicamentosas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_medicamento')->unsigned();
            $table->integer('id_interacao_med')->unsigned();
            $table->timestamps();
            $table->softDeletes();

	        $table->foreign('id_medicamento')->references('id')->on('medicamentos')->onDelete('no action')->onUpdate('no action');
	        $table->foreign('id_interacao_med')->references('id')->on('interacao_medicamentosas')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_interacao_medicamentosas');
    }
}
