<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('list-medicamentos/{search?}/{t?}', 'MemedController@lists')->name('list');
Route::get('list-anvisa/{search?}', 'MemedController@storeAnvisa')->name('list');

Route::name('pharmacies')->namespace('Medicines')->group(function () {
	Route::post('store-medicines', 'MedicinesController@store')->name('store');
	Route::post('update-medicines', 'MedicinesController@update')->name('update');
	Route::get('get-all-medicines/{perPage?}/{search?}', 'MedicinesController@getAllMedicines')->name('all');
	Route::get('get-medicines-detail/{id}', 'MedicinesController@detailsMedicals')->name('detail');
	Route::get('combo-manufacturers/{perPage?}/{search?}', 'MedicinesController@comboManufacturer')->name('detail');
	Route::get('combo-types/{perPage?}/{search?}', 'MedicinesController@comboTypeMedicines')->name('type');
	Route::post('delete-medicines', 'MedicinesController@delete')->name('delete');
});
